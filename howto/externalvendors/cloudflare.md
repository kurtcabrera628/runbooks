# Getting support from Cloudflare

## Issue Tracker

**Cloudflare Vendor Tracker**: https://gitlab.com/gitlab-com/gl-infra/cloudflare/issues

## Contacting support



## Contact Numbers

Should we need to call Cloudflare, we were given these numbers to reach out to for help.

There are a few noted with a # that can only be called from the country specified.  

Local Enterprise phone numbers for each country:

United States: +1 650-353-5922
United Kingdom: +44 808-169-9540
India Toll-Free Number: +91 0008000501934 #
South Korea Toll-Free Number: +82 (00798) 142030193 #
Taiwan Toll-Free Number: +886 (80) 1491362  #
Australia Toll-Free Number: +61 (1800) 491698
New Zealand Toll-Free Number: +64 (80) 0758909
Japan: +81 503-196-5771
Brazil: +55 114-950-8998, +55-11-4949-5922
Canada: +1 647-360-8385
Mexico +52-155-4169-5968
Chile +56-2-2666-5928
Singapore +65 800-321-1182 #
China +86 (10) 85241784
